package vn.com.hung.notetaker.book.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import vn.com.hung.notetaker.book.menus.BNMenu;
import vn.com.hung.notetaker.datatypes.BookRecord;

@SuppressWarnings("serial")
public class BNTakerFrame extends JFrame implements ActionListener{
    
    private ArrayList<BNTakerPanel> noteTakers = new ArrayList<BNTakerPanel>();
    private JTabbedPane tabbedMenu = new JTabbedPane();
    private BNMenu menu;
    private JButton filterBT = new JButton("Search");
    private JTextField filterTF = new JTextField();
    public BNTakerFrame(BookRecord book) {
        
        //noteTaker.loadBook(Configuration.getBookNotePath() + "/" + book.getPath());
        BNTakerPanel noteTaker = new BNTakerPanel(book);
        menu = new BNMenu(noteTaker);
        noteTakers.add(noteTaker);
        tabbedMenu.addTab("Book 1", noteTaker);
        setJMenuBar(menu);
        initComponents();
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(tabbedMenu)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(filterBT)
                        .addComponent(filterTF)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(tabbedMenu)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(filterBT)
                        .addComponent(filterTF)));
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
    }
    
    private void initComponents() {
       tabbedMenu.setTabPlacement(JTabbedPane.BOTTOM);
       tabbedMenu.addChangeListener(new TabChangeListener());
       filterBT.addActionListener(this);
       filterTF.addActionListener(this);
        
        
    }

    public void addBookNoteTaker(BookRecord book) {
        for (BNTakerPanel panel : noteTakers) {
            if (panel.getBook().equals(book)) {
                return;
            }
        }
        BNTakerPanel noteTaker = new BNTakerPanel(book);
        noteTakers.add(noteTaker);
        tabbedMenu.addTab("Book " + noteTakers.size(), noteTaker);
        
    }
    
    public BNTakerPanel getCurrentSheet() {
        return noteTakers.get(tabbedMenu.getSelectedIndex());
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
               BookRecord book = new BookRecord("Graph Theory", "Reinhard Diestel");
               BNTakerFrame frame = new BNTakerFrame(book);
               frame.setVisible(true);
               
            }
        } );
    }
    
    private class TabChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent arg0) {
            System.out.println("Tabb " + tabbedMenu.getSelectedIndex());
            menu.setNoteTaker(noteTakers.get(tabbedMenu.getSelectedIndex()));
        }
        
    }
    
   public void removeAllTab() {
       noteTakers.clear();
       tabbedMenu.removeAll();
   }
   public int getCurrentTab() {
       return tabbedMenu.getSelectedIndex();
   }

@Override
public void actionPerformed(ActionEvent arg0) {
    JComponent source = (JComponent) arg0.getSource();
    if (source.equals(filterBT) || source.equals(filterTF)) {
        System.out.println("search");
        BNTakerPanel panel = noteTakers.get(getCurrentTab());
        if (filterTF.getText().length() == 0) {
            panel.setRowFilter(null);
        } else {
            panel.setRowFilter(RowFilter.regexFilter("(?i)" + filterTF.getText()));
        }
    } 
    
}
}
