package vn.com.hung.notetaker.book.forms;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import vn.com.hung.notetaker.book.components.BNAutoJTFCellEditor;
import vn.com.hung.notetaker.book.components.BNDefaultCellEditor;
import vn.com.hung.notetaker.book.components.BNScrollEditor;
import vn.com.hung.notetaker.book.components.BNTableAction;
import vn.com.hung.notetaker.book.components.BNTableRenderer;
import vn.com.hung.notetaker.book.components.JBNTable;
import vn.com.hung.notetaker.book.components.TableCellSnapshotEditor;
import vn.com.hung.notetaker.book.datamodels.BNTableModel;
import vn.com.hung.notetaker.book.menus.BNRightClickMenu;
import vn.com.hung.notetaker.config.Conf;
import vn.com.hung.notetaker.datatypes.BookRecord;
import vn.com.hung.notetaker.utils.FontPool;

@SuppressWarnings("serial")
public class BNTakerPanel extends JPanel { //JFrame{
    
    private BookRecord book;
    private String noteTitle = "Title";
    
    private BNTableModel model = new BNTableModel();
    private TableRowSorter<TableModel> sorter;
    private JBNTable noteTable = new JBNTable();
    private JLabel bookTitleLabel = new JLabel();
    private JLabel editLabel = new JLabel("[0:0] f(x): ");
    private TableCellSnapshotEditor cellSnapshot = new TableCellSnapshotEditor(100);
    
    public BNTakerPanel(BookRecord book) {
        this.book = book;
        noteTitle = book.getName() + " - " + book.getAuthor(); 
        
        initComponents();
        bookTitleLabel.setText(noteTitle);
        bookTitleLabel.setFont(FontPool.LARGE_SERIF_BOLD);
        JScrollPane scrollPane = new JScrollPane(noteTable);
        
        cellSnapshot.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
               if (cellSnapshot.getColumn() == 1) {
                    BNScrollEditor cellEditor = (BNScrollEditor) noteTable
                            .getColumnModel().getColumn(1).getCellEditor();
                    cellEditor.setText(cellSnapshot.getText());

               } else {
                   BNDefaultCellEditor cellEditor = (BNDefaultCellEditor) noteTable
                           .getColumnModel().getColumn(cellSnapshot.getColumn()).getCellEditor();
                   cellEditor.setText(cellSnapshot.getText());
                   
               }
               
            }
        });
        
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
       
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(bookTitleLabel)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(editLabel)
                        .addComponent(cellSnapshot))
                .addComponent(scrollPane));
        
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(bookTitleLabel)
                .addGap(30)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(editLabel)
                        .addComponent(cellSnapshot))
                .addComponent(scrollPane));
        loadBook(Conf.getBookNotePath() + "/" + book.getPath());
        
         }
    
    private void initComponents() {
        noteTable.setModel(model);
        sorter = new TableRowSorter<TableModel>(model);
        noteTable.setRowSorter(sorter);
        noteTable.setFont(FontPool.MEDIUM_SERIF_BOLD);
        for (int col = 0; col < noteTable.getColumnCount(); col++) {
            noteTable.getColumnModel().getColumn(col).setCellRenderer(new BNTableRenderer());
            if (col == 1) {
                noteTable.getColumnModel().getColumn(col).setCellEditor(new BNScrollEditor(this));
            } else if (col == 2) {
                noteTable.getColumnModel().getColumn(col).setCellEditor(new BNAutoJTFCellEditor(this));
             } else {
                noteTable.getColumnModel().getColumn(col).setCellEditor(new BNDefaultCellEditor(this));
                
            }
        }
        
        noteTable.addMouseListener(new BookNoteTableMouseListener());
        InputMap inputMap = noteTable.getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        ActionMap action = noteTable.getActionMap();
        // ctrl + s
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), BNTableAction.CTRL_S);
        action.put(BNTableAction.CTRL_S, BNTableAction.newInstance(BNTableAction.CTRL_S));
        // ctrl + shift + s
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK),
                BNTableAction.CTRL_SHIFT_S);
        action.put(BNTableAction.CTRL_SHIFT_S, BNTableAction.newInstance(BNTableAction.CTRL_SHIFT_S));
        
        
    }

    public JTextField getSnapshotTextField() {
        return cellSnapshot;  
    }
    
    public JLabel getSnapshotLabel() {
        return editLabel;
    }
    
    public JTable getNoteTable() {
        return noteTable;
    }
    
    public BookRecord getBook() {
        return book;
    }
    
    private class BookNoteTableMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                Point p = e.getPoint();
                int row = noteTable.rowAtPoint(p);
                int column = noteTable.columnAtPoint(p);
                cellSnapshot.setColumn(column);
                cellSnapshot.setRow(row);
                cellSnapshot.setText(noteTable.getValueAt(row, column).toString());
                editLabel.setText("[" + row + ":" + column + "] f(x):");
                
            } else if (SwingUtilities.isRightMouseButton(e)) {
                Point p = e.getPoint();
                int row = noteTable.rowAtPoint(p);
                int column = noteTable.columnAtPoint(p);
                noteTable.setSelectedCell(row, column);
                BNRightClickMenu pmenu = new BNRightClickMenu(noteTable);
                pmenu.show(e.getComponent(), e.getX(), e.getY());
               
            }
            
        }
    }
    
    public void loadBook(String path) {
        ((BNTableModel) noteTable.getModel()).loadCSV(path);
        noteTable.updateUI();
        sorter.allRowsChanged();
    }
    
    public void setRowFilter(RowFilter filter) {
        sorter.setRowFilter(filter);
    }
    
   
    
}
