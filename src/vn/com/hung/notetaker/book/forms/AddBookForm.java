package vn.com.hung.notetaker.book.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.book.datamodels.BookViewTableModel;

@SuppressWarnings("serial")
public class AddBookForm extends AddForm implements ActionListener{
    
    private JLabel bookAuthorLabel = new JLabel("Book's Author");
    private JTextField bookAuthorField = new JTextField(20);
    private JLabel bookNameLabel = new JLabel("Book's Name");
    private JTextField bookTextField = new JTextField(20);
    private JLabel statusLabel = new JLabel("Status");
    private JTextField statusField = new JTextField(20);
    private JButton okButton = new JButton("Ok");
    
    public AddBookForm(JTable bookTable) {
         super(bookTable);
         okButton.addActionListener(this);
         
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                        .addComponent(bookNameLabel)
                        .addComponent(bookAuthorLabel)
                        .addComponent(statusLabel)
                        .addComponent(okButton))
                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                        .addComponent(bookTextField)
                        .addComponent(bookAuthorField)
                        .addComponent(statusField)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(bookNameLabel)
                        .addComponent(bookTextField))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(bookAuthorLabel)
                        .addComponent(bookAuthorField))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(statusLabel)
                        .addComponent(statusField))
                .addGap(10)
                .addComponent(okButton));
        setVisible(true);
        setLocation(500, 200);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                AddBookForm form =  new AddBookForm(null);
                form.setLocation(500, 200);
            }
        });
    }
    
    public void setMode(int mode) {
        this.mode = mode;
        if (mode == ADD_MODE) {
            okButton.setText("Add");
        } else if (mode == EDIT_MODE) {
            okButton.setText("Done");
        }
    }
    
    public void setBookNameField(String name) {
        this.bookTextField.setText(name);
    }
    
    public void setAuthorField(String author) {
        this.bookAuthorField.setText(author);
    }
    
    public void setStatusField(String status) {
        this.statusField.setText(status);
    }



    @Override
    public void setData(Vector<Object> data) {
        setBookNameField(data.get(0).toString());
        setAuthorField(data.get(1).toString());
        setStatusField(data.get(2).toString());
    }



    @Override
    public void actionPerformed(ActionEvent arg0) {
        String author = bookAuthorField.getText();
        if (author == null || author.length() == 0) {
            author = "N/A";
        }
        String status = statusField.getText();
        if (status == null || status.length() == 0) {
            status = "N/A";
        }
        String bookName = bookTextField.getText();
        if (bookName == null || bookName.length() == 0) {
            bookName = "";
            author = "";
            status = "";
        }
        
        Vector<Object> data = new Vector<Object>();
        data.add(bookName);
        data.add(author);
        data.add(status);
        if (mode == ADD_MODE) {
            ((BookViewTableModel) table.getModel()).addRow(data, table.getRowCount());
        } else if (mode == EDIT_MODE) {
            ((BookViewTableModel) table.getModel()).replaceRow(data, table.getSelectedRow());
        }
        dispose();
    }

}
