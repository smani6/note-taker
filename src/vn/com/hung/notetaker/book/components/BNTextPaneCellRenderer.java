package vn.com.hung.notetaker.book.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.TextArea;

import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.AbstractDocument;

import vn.com.hung.notetaker.book.datamodels.BNTableModel;
import vn.com.hung.notetaker.book.forms.BNTakerPanel;
import vn.com.hung.notetaker.demos.TextPaneTest;
import vn.com.hung.notetaker.utils.FontPool;
import vn.com.hung.notetaker.utils.NoteTextPane;
import vn.com.hung.notetaker.utils.StringUtils;

public class BNTextPaneCellRenderer extends JTextPane implements TableCellRenderer{
    
 private BNTakerPanel noteTaker;
    
  public BNTextPaneCellRenderer(BNTakerPanel noteTaker) {
        super();
        this.noteTaker = noteTaker;
        setFont(FontPool.MEDIUM_SERIF_BOLD);
    }


    @Override
    public Component getTableCellRendererComponent(
        JTable jTable, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
        
        if(isSelected) {
           setBackground(Color.yellow);
        } else {
            setBackground(Color.white);
        }
        setText(((JTextPane) value).getText());
        return this;  
       
    }

}
