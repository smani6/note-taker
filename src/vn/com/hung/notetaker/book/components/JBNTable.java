package vn.com.hung.notetaker.book.components;

import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

@SuppressWarnings("serial")
public class JBNTable extends JTable{
    
    private Font originalFont;
    private int originalRowHeight;
    private float zoomFactor = 1.0f;
    private Point selectedCell = new Point();
    
    public JBNTable() {
        super();
    }
    public JBNTable(int numRows, int numColumns) {
        super(numRows, numColumns);
    }
    public JBNTable(Object[][] rowData, Object[] columnNames) {
        super(rowData, columnNames);
    }
    
    public JBNTable(TableModel tm) {
        super(tm);
    }
    public JBNTable(TableModel dm, TableColumnModel cm) {
        super(dm, cm);
    }
    public JBNTable(Vector<?> rowData, Vector<?> columnNames) {
        super(rowData, columnNames);
    }
    
    @Override
    public void setFont(Font font)
    {
        originalFont = font;
        // When setFont() is first called, zoomFactor is 0.
        if (zoomFactor != 0.0f && zoomFactor != 1.0f)
        {
            float scaledSize = originalFont.getSize2D() * zoomFactor;
            font = originalFont.deriveFont(scaledSize);
        }

        super.setFont(font);
    }
    
    @Override
    public void setRowHeight(int rowHeight)
    {
        originalRowHeight = rowHeight;
        // When setRowHeight() is first called, zoomFactor is 0.
        if (zoomFactor != 0.0 && zoomFactor != 1.0)
            rowHeight = (int) Math.ceil(originalRowHeight * zoomFactor);

        super.setRowHeight(rowHeight);
    }
    
    public float getZoom()
    {
        return zoomFactor;
    }
    
    public void setZoom(float zoomFactor)
    {
        if (this.zoomFactor == zoomFactor)
            return;

        if (originalFont == null)
            originalFont = getFont();
        if (originalRowHeight == 0)
            originalRowHeight = getRowHeight();

        float oldZoomFactor = this.zoomFactor;
        this.zoomFactor = zoomFactor;
        Font font = originalFont;
        if (zoomFactor != 1.0)
        {
            float scaledSize = originalFont.getSize2D() * zoomFactor;
            font = originalFont.deriveFont(scaledSize);
        }

        super.setFont(font);
        super.setRowHeight((int) Math.ceil(originalRowHeight * zoomFactor));
        getTableHeader().setFont(font);

        firePropertyChange("zoom", oldZoomFactor, zoomFactor);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
    {
        Component comp = super.prepareRenderer(renderer, row, column);
        comp.setFont(this.getFont());
        return comp;
    }

    @Override
    public Component prepareEditor(TableCellEditor editor, int row, int column)
    {
        Component comp = super.prepareEditor(editor, row, column);
        comp.setFont(this.getFont());
        return comp;
    }
    
    public void setSelectedCell(int row, int col) {
        this.selectedCell = new Point(row, col);
    }
    
    public Point getSelectedCell() {
        return selectedCell;
    }
    
    

}
