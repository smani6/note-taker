package vn.com.hung.notetaker.book.components;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableCellEditor;

import vn.com.hung.notetaker.book.forms.BNTakerPanel;
import vn.com.hung.notetaker.utils.FontPool;

@SuppressWarnings("serial")
public class BNScrollEditor extends AbstractCellEditor implements TableCellEditor{

    JTextPane area = new JTextPane();
    JScrollPane pane = new JScrollPane(area);
    BNTakerPanel noteTaker;
    
    public BNScrollEditor(BNTakerPanel noteTaker) {
        this.noteTaker = noteTaker;
        area.setFont(FontPool.MEDIUM_SERIF_BOLD);
        
    }
    @Override
    public Object getCellEditorValue() {
        return area.getText();
        
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSlected, int row, int column) {
        area.setText(value.toString());
        TableCellSnapshotEditor snapshot = (TableCellSnapshotEditor) noteTaker.getSnapshotTextField();
        snapshot.setText(value.toString());
        snapshot.setRow(row);
        snapshot.setColumn(column);
        noteTaker.getSnapshotLabel().setText("[" + row + ":" + column + "] f(x):");
        return pane;
    }
    
    public void setText(String text) {
        area.setText(text);
    }
    
    

}
