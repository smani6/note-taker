package vn.com.hung.notetaker.book.components;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.TableCellEditor;

@SuppressWarnings("serial")
public class BNTableEditor extends AbstractCellEditor implements TableCellEditor{
    
    JComponent component = new JTextArea();
    

    @Override
    public Object getCellEditorValue() {
        return (Object) ((JTextArea) component).getText();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSlected, int row, int column) {
        int columnWidth = table.getColumnModel().getColumn(column).getWidth();
        ((JTextArea) component).setText(value.toString());
        ((JTextArea) component).setLineWrap(true);
        ((JTextArea) component).setWrapStyleWord(true);
        ((JTextArea) component).setColumns(columnWidth);
        ((JTextArea) component).setRows(table.getRowHeight(row));
        return component;
    }

}
