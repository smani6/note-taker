package vn.com.hung.notetaker.book.components;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import vn.com.hung.notetaker.book.forms.BNTakerPanel;
import vn.com.hung.notetaker.utils.AutoJTextField;

public class BNAutoJTFCellEditor extends AbstractCellEditor implements TableCellEditor{

    private BNTakerPanel noteTaker;
    private AutoJTextField cellEditor = new AutoJTextField();
    
    public BNAutoJTFCellEditor(BNTakerPanel noteTaker) {
        this.noteTaker = noteTaker;
    }
    @Override
    public Object getCellEditorValue() {
        return cellEditor.getText();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        cellEditor.setText(value.toString());
        cellEditor.setFont(table.getFont());
        TableCellSnapshotEditor snapshot = (TableCellSnapshotEditor) noteTaker.getSnapshotTextField();
        snapshot.setText(value.toString());
        snapshot.setRow(row);
        snapshot.setColumn(column);
        noteTaker.getSnapshotLabel().setText("[" + row + ":" + column + "] f(x):");
        return cellEditor;
    }

}
