package vn.com.hung.notetaker.book.components;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class BNTableAction extends AbstractAction{
    public static final String CTRL_S = "Save";
    public static final String CTRL_SHIFT_S = "SaveAs";
    
    private String actionKey;
    public BNTableAction(String action) {
        this.actionKey = action;
    }
    
    public void actionPerformed(ActionEvent arg0) {
        if (actionKey.equals(CTRL_S)) {
            System.out.println("do save");
        } else if (actionKey.equals(CTRL_SHIFT_S)) {
            System.out.println("do save as");
        }
    }
    
    public static BNTableAction newInstance(String actionKey) {
        return new BNTableAction(actionKey);
    }

}
