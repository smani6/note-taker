package vn.com.hung.notetaker.book.components;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public class TableCellSnapshotEditor extends JTextField {
    
    private int row = 0;
    private int column = 0;
    
    public TableCellSnapshotEditor() { }
    
    public TableCellSnapshotEditor(int row, int column) {
        this.row = row;
        this.column = column;
    }
    public TableCellSnapshotEditor(int width) {
        super(width);
    }
    
    public int getRow() {
        return row;
    }
    public int getColumn() {
        return column;
    }
    public void setRow(int row) {
        this.row = row;
    }
    
    public void setColumn(int column) {
        this.column = column;
    }

}
