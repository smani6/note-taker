package vn.com.hung.notetaker.book.components;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;

import vn.com.hung.notetaker.book.forms.BNTakerPanel;

public class ZoomSliderForm extends JFrame{
    
    BNTakerPanel noteTaker;
    private JSlider zoomSlider = new JSlider(JSlider.HORIZONTAL, 0, 200, 100);
    private JLabel zoomLabel = new JLabel("Zoom");
    private JButton button = new JButton("Ok");
   
    public ZoomSliderForm(BNTakerPanel noteTaker) {
        this.noteTaker = noteTaker;
        initComponents();
        BoxLayout layout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
        Container pane = getContentPane();
        pane.setLayout(layout);
        pane.add(zoomLabel);
        pane.add(zoomSlider);
        pane.add(button);
        button.requestFocus();
        setBounds(400, 200, 400, 100);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    private void initComponents() {
        zoomSlider.setPaintLabels(true);
        zoomSlider.setPaintTicks(true);
        zoomSlider.setMajorTickSpacing(50);
        zoomSlider.setMinorTickSpacing(10);
        
        zoomLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        zoomLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 15, 0));
        zoomSlider.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                int value = zoomSlider.getValue();
                ((JBNTable)noteTaker.getNoteTable()).setZoom((float) value / 100f);
                setVisible(false);
            }
        });
        
    }
    
    public static void main(String[] args) {
        //new ZoomSliderForm();
    }
    
    

}
