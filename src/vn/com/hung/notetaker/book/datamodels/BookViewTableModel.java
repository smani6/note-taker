package vn.com.hung.notetaker.book.datamodels;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;

import com.google.common.io.Closeables;
import com.google.common.io.Files;

import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.abstractModels.NoteTableModel;
import vn.com.hung.notetaker.book.forms.AddBookForm;
import vn.com.hung.notetaker.datatypes.BookRecord;
import vn.com.hung.notetaker.datatypes.BookTags;
import vn.com.hung.notetaker.datatypes.PaperRecord;
import vn.com.hung.notetaker.utils.BookDataLoader;

@SuppressWarnings({ "unchecked", "serial" })
public class BookViewTableModel extends NoteTableModel {
    
    public BookViewTableModel() {
        String[] columnNames = {"Book's Name", "Author", "Status"};
        this.columnNames = columnNames;
        for (int i = 0; i < 50; i++) {
            Vector<Object> rowData = BookRecord.getEmptyRow();
            data.add(rowData);
        }  
    }
    public BookViewTableModel(String[] columnNames) {
        this.columnNames = columnNames;
        for (int i = 0; i < 50; i++) {
            Vector<Object> rowData = BookRecord.getEmptyRow();
            data.add(rowData);
        }        
    }

    @Override
    public void saveToCSV(String fileName) throws IOException {
        BufferedWriter bw = Files.newWriter(new File(fileName), Charset.forName("UTF-8"));
        if (data == null || data.size() == 0) {
            Closeables.closeQuietly(bw);
            return;
        }
        bw.write(BookTags.ROOT_START + "\n");
        for (Vector<Object> datum : data) {
            bw.write(BookTags.BOOK_TAG_START + "\n");
            Iterator<Object> it = datum.iterator();
            for (int i = 0; it.hasNext(); i++) {
               switch (i) {
               case 0:
                   bw.write(BookTags.TITLE_TAG_START);
                   bw.write(it.next().toString());
                   bw.write(BookTags.TITLE_TAG_END);
                   break;
               case 1: 
                   bw.write(BookTags.AUTHOR_TAG_START);
                   bw.write(it.next().toString());
                   bw.write(BookTags.AUTHOR_TAG_END);
                   break;
               case 2: 
                   bw.write(BookTags.STATUS_TAG_START);
                   bw.write(it.next().toString());
                   bw.write(BookTags.STATUS_TAG_END);
            default:
                break;
                           }
                   
            }
            bw.write(BookTags.BOOK_TAG_END + "\n");
        }
        bw.write(BookTags.ROOT_END  + "\n");
        Closeables.closeQuietly(bw);
    }

    @Override
    public void loadCSV(String fileName) throws IOException {
        System.out.println("load");
        this.filePath = fileName;
        loader = new BookDataLoader(filePath);
        loader.load();
        List<BookRecord> bookList = ((BookDataLoader)loader).getBookList();
        data.clear();
        
        for (BookRecord book : bookList) {
            Vector<Object> datum = new Vector<Object>();
            datum.add(book.getName());
            datum.add(book.getAuthor());
            datum.add(book.getStatus());
            data.add(datum);
        }
    }

    @Override
    public int getStatusRowId() {
        return 2;
    }
    @Override
    public AddForm getEditForm(JTable table) {
        return new AddBookForm(table);
    }

}
