package vn.com.hung.notetaker.book.menus;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;

import vn.com.hung.notetaker.book.components.ZoomSliderForm;
import vn.com.hung.notetaker.book.datamodels.BNTableModel;
import vn.com.hung.notetaker.book.forms.BNTakerPanel;
import vn.com.hung.notetaker.utils.FontChooser;
import vn.com.hung.notetaker.utils.XMLFilter;

public class BNMenu extends JMenuBar implements ActionListener{
    
    private BNTakerPanel noteTaker;
   // private JTable table;
    
    private JMenu fileMenu = new JMenu("File");
    private JMenuItem loadItem = new JMenuItem("Load Data");
    private JMenuItem saveAsItem = new JMenuItem("Save As");
    
    private JMenu viewMenu = new JMenu("View");
    private JMenuItem fontItem = new JMenuItem("Font", KeyEvent.VK_F);
    private JMenuItem zoomItem = new JMenuItem("Zoom");
    
    
    public BNMenu(BNTakerPanel noteTaker) {
        this.noteTaker = noteTaker;
       // this.table = noteTaker.getNoteTable();
        add(fileMenu);
        add(viewMenu);
        
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(loadItem);
        fileMenu.add(saveAsItem);
        
        loadItem.setMnemonic(KeyEvent.VK_L);
        loadItem.setName("load");
        loadItem.addActionListener(this);
        
        saveAsItem.setMnemonic(KeyEvent.VK_S);
        saveAsItem.setName("saveAs");
        saveAsItem.addActionListener(this);
        
        viewMenu.setMnemonic(KeyEvent.VK_V);
        viewMenu.add(fontItem);
        viewMenu.add(zoomItem);
        
        fontItem.setName("font");
        fontItem.addActionListener(this);
        zoomItem.setName("zoom");
        zoomItem.addActionListener(this);
    }
    
    public void setNoteTaker(BNTakerPanel noteTaker) {
        this.noteTaker = noteTaker;
        
    }
    public void actionPerformed(ActionEvent e) {
        String componentName = ((JComponent) e.getSource()).getName();
        JTable table = noteTaker.getNoteTable();
        if (componentName.equals(loadItem.getName())) {
            // process choose file
            JFileChooser c = new JFileChooser();
            c.setFileFilter(new XMLFilter());
            int returnValue = c.showOpenDialog(getParent());
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = c.getSelectedFile();
                ((BNTableModel) table.getModel()).loadCSV(selectedFile.getAbsolutePath());
                table.updateUI();
              }
        } else if (componentName.equals(saveAsItem.getName())) {
            // process save as 
            JFileChooser c = new JFileChooser();
            c.setFileFilter(new XMLFilter());
            int returnValue = c.showSaveDialog(getParent());
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                String fileName = c.getSelectedFile().getPath();
                if (!fileName.endsWith(".xml")) {
                    fileName += ".xml";
                }
                try {
                    ((BNTableModel) table.getModel()).saveToCSV(fileName);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        } else if (componentName.equals(fontItem.getName())) {
            // process font chooser
            Font font = null;
            font = FontChooser.showDialog(null, null, null);
            if (font != null) {
                System.out.println(FontChooser.fontString(font));
                table.setFont(font);
            }
        } else if (componentName.equals(zoomItem.getName())) {
           ZoomSliderForm slider = new ZoomSliderForm(noteTaker);
           slider.setTitle("Zoom Chooser");
           slider.setVisible(true);
        } 
    }


}
