package vn.com.hung.notetaker.utils;

import java.awt.FontMetrics;

public class StringUtils {
    
    public static int countLines(String str){
        String[] lines = str.split("\r\n|\r|\n");
        return  lines.length;
     }
    public static String[] splitLines(String str){
        String[] lines = str.split("\r\n|\r|\n");
        return  lines;
     }
    
    public static int getStringHeight(String str, FontMetrics font, int baseWidth, int baseHeight) {
        int rowHeightCount = 0;
        if (str.length() < 2 || str == null) return baseHeight;
        String[] lines = splitLines(str);
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].length() == 0 || lines[i] == null) {
                rowHeightCount++;
            } else {
                rowHeightCount += font.stringWidth(lines[i])/baseWidth + 1;
            }
        }
        return rowHeightCount*baseHeight;
    }


}
