package vn.com.hung.notetaker.utils;

import java.awt.Font;

public class FontPool {
    
    public static final Font SMALL_SERIF_PLAIN = new Font("Serif", Font.PLAIN, 5);
    public static final Font MEDIUM_SERIF_PLAIN = new Font("Serif", Font.PLAIN, 12);
    public static final Font LARGE_SERIF_PLAIN = new Font("Serif", Font.PLAIN, 20);
    public static final Font EXTRA_LARGE_SERIF_PLAIN = new Font("Serif", Font.PLAIN, 40);
    
    public static final Font SMALL_SERIF_BOLD = new Font("Serif", Font.BOLD, 5);
    public static final Font MEDIUM_SERIF_BOLD = new Font("Serif", Font.BOLD, 12);
    public static final Font LARGE_SERIF_BOLD = new Font("Serif", Font.BOLD, 20);
    public static final Font EXTRA_LARGE_SERIF_BOLD = new Font("Serif", Font.BOLD, 40);
    
    
}
