package vn.com.hung.notetaker.utils;

public class SpecialChars {
    
    public static final char SPECIAL_AND = '&';
    public static final char SPECIAL_SHARP = '#';
    public static final char SPECIAL_LESS_THAN = '<';
    public static final char SPECIAL_LARGER_THAN = '>';
    
    public static final String SPECIAL_AND_CODE = "&#x26;";
    public static final String SPECIAL_LESS_THAN_CODE = "&lt;";
    public static final String SPECIAL_LARGER_THAN_CODE = "&gt;";
    
    public static String regularize(String text) {
        if (text == null) {
            return ""; 
        }
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c1 = text.charAt(i);
            if (c1 == SPECIAL_AND) {
                if ( i < text.length() -1) {
                    char c2 = text.charAt(i+1); 
                        if (c2 == SPECIAL_SHARP) {
                            out.append(c1);
                        } else {
                            out.append(SPECIAL_AND_CODE);
                        }
                }
            } else if (c1 == SPECIAL_LESS_THAN) {
                out.append(SPECIAL_LESS_THAN_CODE);
            } else if (c1 == SPECIAL_LARGER_THAN) {
                out.append(SPECIAL_LARGER_THAN_CODE);
                
            }else {
                out.append(c1);
            }
        }
        return out.toString();
    }
    public static void main(String[] args) {
        System.out.println(regularize("anh>eem"));
    }

}
