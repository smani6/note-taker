package vn.com.hung.notetaker.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class XMLFilter extends FileFilter{

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        if (f.getName().endsWith(".xml")) {
            return true;
        } else return false;
    }

    @Override
    public String getDescription() {
        return "*.xml";
    }

}
