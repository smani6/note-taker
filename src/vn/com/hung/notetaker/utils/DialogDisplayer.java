package vn.com.hung.notetaker.utils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DialogDisplayer {
    
    public static void showDefaultInforDialog(String smg) {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, smg, "Information", JOptionPane.INFORMATION_MESSAGE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setLocation(500, 200);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
    
    public static void showDefaultErrorDialog(String smg) {
        JFrame frame = new JFrame();
        JOptionPane.showMessageDialog(frame, smg, "Error", JOptionPane.ERROR_MESSAGE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setLocation(500, 200);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}
