package vn.com.hung.notetaker.utils;

import java.awt.Color;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.InputMap;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;

@SuppressWarnings("serial")
public class NoteTextPane extends JTextPane{
    
    public NoteTextPane() {
        init();
    }
    
   private void init() {
       addMouseListener(new TextPaneMouseListener());
       addBindings();
       
   }
   
   private void addBindings() {
       InputMap inputMap = getInputMap();
       // ctrl + r = red
       KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK);
       inputMap.put(key, new StyledEditorKit.ForegroundAction("red", Color.red));
       // ctrl + y = yellow
       key = KeyStroke.getKeyStroke(KeyEvent.VK_Y, Event.CTRL_MASK);
       inputMap.put(key, new StyledEditorKit.ForegroundAction("yellow", Color.yellow));
       // ctrl + b = bold
       key = KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK);
       inputMap.put(key, new StyledEditorKit.BoldAction());
       // ctrl + i = italic
       key = KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK);
       inputMap.put(key, new StyledEditorKit.ItalicAction());
       // ctrl + u = underline
       key = KeyStroke.getKeyStroke(KeyEvent.VK_U, Event.CTRL_MASK);
       inputMap.put(key, new StyledEditorKit.UnderlineAction());
       
       
   }
    
    private class TextPaneMouseListener extends MouseAdapter {
        private JPopupMenu pmenu = new JPopupMenu("Menu");
        private JMenu   fontSize = new JMenu("Font Size");
        private JMenu   fontColor = new JMenu("Highlight");
        private JMenu   fontFamily = new JMenu("Font Family");
        
        public TextPaneMouseListener() {
            //pmenu.add(new StyledEditorKit.ForegroundAction("Red", Color.red));
            pmenu.add(fontSize);
            fontSize.add(new StyledEditorKit.FontSizeAction("12", 12));
            fontSize.add(new StyledEditorKit.FontSizeAction("14", 14));
            fontSize.add(new StyledEditorKit.FontSizeAction("16", 16));
            fontSize.add(new StyledEditorKit.FontSizeAction("18", 18));
            
            pmenu.add(fontColor);
            fontColor.add(new StyledEditorKit.ForegroundAction("Red", Color.red));
            fontColor.add(new StyledEditorKit.ForegroundAction("Blue", Color.blue));
            fontColor.add(new StyledEditorKit.ForegroundAction("Yellow", Color.yellow));
            fontColor.add(new StyledEditorKit.ForegroundAction("Black", Color.black));
            fontColor.addSeparator();
            fontColor.add(new StyledEditorKit.UnderlineAction());
            fontColor.add(new StyledEditorKit.BoldAction());
            fontColor.add(new StyledEditorKit.ItalicAction());
            
            pmenu.add(fontFamily);
            fontFamily.add(new StyledEditorKit.FontFamilyAction("SansSerif", "SansSerif"));
            fontFamily.add(new StyledEditorKit.FontFamilyAction("Serif", "Serif"));
            fontFamily.add(new StyledEditorKit.FontFamilyAction("Times New Roman", "Times New Roman"));
            fontFamily.add(new StyledEditorKit.FontFamilyAction("Arial", "Arial"));
            
          
        }
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                System.out.println("left");
            } else if (SwingUtilities.isRightMouseButton(e)) {
                pmenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }
    
    public AbstractDocument getDoccument() {
        StyledDocument styledDoc = getStyledDocument();

        if (styledDoc instanceof AbstractDocument) {
            return (AbstractDocument) styledDoc;
        } else {
            System.err
                    .println("Text pane's document isn't an AbstractDocument!");
            System.exit(-1);
            return null;
        }
    }

}
