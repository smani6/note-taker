package vn.com.hung.notetaker.utils;

public abstract class AbstractLoader {
    
    protected String path;
    
    public AbstractLoader() { }
    public AbstractLoader(String path) {
        this.path = path;
    }
    
    public abstract void load() ;
    
    public void setPath(String path) {
        this.path = path;
    }
    
    

}
