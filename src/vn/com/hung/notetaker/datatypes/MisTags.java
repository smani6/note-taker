package vn.com.hung.notetaker.datatypes;

public class MisTags {
    
    public static final String ROOT_TAG_START = "<Miscellaneous>";
    public static final String ROOT_TAG_END = "</Miscellaneous>";
    
    public static final String PAGE_TAG_START = "<misc>";
    public static final String PAGE_TAG_END = "</misc>";
    public static final String PAGE_TAG = "misc";
    
    public static final String CONTENT_TAG_START = "<content>";
    public static final String CONTENT_TAG_END = "</content>";
    public static final String CONTENT_TAG= "content";
    
    public static final String TAGS_TAG_START = "<tags>";
    public static final String TAGS_TAG_END = "</tags>";
    public static final String TAGS_TAG = "tags";
    
    public static final String LINK_TAG_START = "<link>";
    public static final String LINK_TAG_END = "</link>";
    public static final String LINK_TAG = "link";
    

}
