package vn.com.hung.notetaker.datatypes;

import java.util.Vector;

public class BookRecord {
    
    private String name;
    private String author = "N/A";
    private String status = "N/A";
    private String location = "N/A";
    
    public BookRecord() { }
    
    public BookRecord(String name, String author) {
        this.name = name;
        this.author = author;
    }
    public BookRecord(String name, String author, String status) {
        this.name = name;
        this.author = author;
        this.status = status;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getName() {
        return this.name;
    }
    public String getAuthor() {
        return this.author;
    }
    public String getStatus() {
        return this.status;
    }
    
    public String getPath() {
        return name + "_" + author + ".xml";
    }
    
    public boolean equals(BookRecord otherBook) {
        if (name.equalsIgnoreCase(otherBook.getName())
                && author.equalsIgnoreCase(otherBook.getAuthor())) {
            return true;
        } else {
            return false;
        }
    }
    
    public static Vector<Object> getEmptyRow() {
        Vector<Object> rowData = new Vector<Object>();
        rowData.add(" ");
        rowData.add(" ");
        rowData.add(" ");
        return rowData;
    }

}
