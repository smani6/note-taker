package vn.com.hung.notetaker.datatypes;

public class BookTags {
    public static final String ROOT_START = "<BOOKS>";
    public static final String ROOT_END = "</BOOKS>";
    
    public static final String BOOK_TAG = "book";
    public static final String BOOK_TAG_START = "<book>";
    public static final String BOOK_TAG_END = "</book>";
    
    public static final String TITLE_TAG = "title";
    public static final String TITLE_TAG_START = "<title>";
    public static final String TITLE_TAG_END = "</title>";
    
    public static final String AUTHOR_TAG = "author";
    public static final String AUTHOR_TAG_START = "<author>";
    public static final String AUTHOR_TAG_END = "</author>";
    
    public static final String NOTE_TAG = "note";
    public static final String NOTE_TAG_START = "<note>";
    public static final String NOTE_TAG_END = "</note>";
    
    public static final String TERMS_TAG = "terms";
    public static final String TERMS_TAG_START = "<terms>";
    public static final String TERMS_TAG_END = "</terms>";
    
    public static final String CONTENTS_TAG = "contents";
    public static final String CONTENTS_TAG_START = "<contents>";
    public static final String CONTENTS_TAG_END = "</contents>";
    
    public static final String TAGS_TAG = "tags";
    public static final String TAGS_TAG_START = "<tags>";
    public static final String TAGS_TAG_END = "</tags>";
    
    public static final String STATUS_TAG = "status";
    public static final String STATUS_TAG_START = "<status>";
    public static final String STATUS_TAG_END = "</status>";
    
    public static final String MISC_TAG = "misc";
    public static final String MISC_TAG_START = "<misc>";
    public static final String MISC_TAG_END = "</misc>";
    
    public static String getAlignment(int align) {
        String out = "";
        for (int i = 0; i < align; i++) {
            out += " ";
        }
        return out;
    }

}
