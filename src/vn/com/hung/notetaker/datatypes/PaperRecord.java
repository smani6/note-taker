package vn.com.hung.notetaker.datatypes;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Vector;

import com.google.common.io.Closeables;
import com.google.common.io.Files;

import vn.com.hung.notetaker.config.Conf;
import vn.com.hung.notetaker.utils.DialogDisplayer;
import vn.com.hung.notetaker.utils.Tags;

public class PaperRecord {
    
    private String title = "";
    private String status = "";
    private String authors = ""; // comma-separated
    private String link = "";
    private String conference = "";
    private String tags = "";
    private String content = "";
    
    public PaperRecord() {
        super();
    }
    
    public PaperRecord(String title, String authors) {
        this.title = title;
        this.authors = authors;
    }
    
    public PaperRecord(String title, String authors, String link) {
        this.title = title;
        this.authors = authors;
        this.link = link;
    }
    
    public PaperRecord(String title, String authors, String link, String conference) {
        this.title = title;
        this.authors = authors;
        this.link = link;
        this.conference = conference;
    }
    public PaperRecord(String title, String authors, String link, String conference, String tags) {
        this.title = title;
        this.authors = authors;
        this.link = link;
        this.conference = conference;
        this.tags = tags;
        Tags.processTags(tags);
        
    }
    
    
    public void setTitle(String title) {
        this.title = title;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setAuthors(String authors) {
        this.authors = authors;
    }
    
    public void setLink(String link) {
        this.link = link;
    }
    
    public void setConference(String conference) {
        this.conference = conference;
    }
    
    public void setTags(String tags) {
        this.tags = tags;
        Tags.processTags(tags);
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    public String getTitle() {
        return title;
    }
    public String getStatus() {
        return status;
    }
    public String getAuthors() {
        return authors;
    }
    
    public String getLink() {
        return link;
    }
    
    public String getConference() {
        return conference;
    }
    
    public String getTags() {
        return tags;
    }
    
    public String getContent() {
        return content;
    }
    public String getFileName() {
        return getShiftMinusSeparated(title) +  "_" + getShiftMinusSeparated(authors) + ".html";
    }
    
    private String getShiftMinusSeparated(String t) {
        String[] ts = t.split(" ");
        String output = "";
        for (String tss : ts) {
            output += tss + "_";
        }
        output = output.substring(0, output.length()-1);
        return output;
    }
    public String readContent() {
        String out = "";
        File contentFile = new File(Conf.getPaperNotePath()
                + getFileName());
        System.out.println(contentFile);
        if (contentFile.exists()) {
            BufferedReader reader = null;
            try {
                reader = Files.newReader(contentFile, Charset.forName("UTF-8"));
                String strLine;
                while ((strLine = reader.readLine()) != null) {
                    out += strLine + "\n";
                }
            } catch (IOException e) {
                DialogDisplayer.showDefaultErrorDialog("System do not find data file");
            } finally {
                Closeables.closeQuietly(reader);
            }

        }
        return out;
    }
    public static Vector<Object> getEmptyRow() {
        Vector<Object> rowData = new Vector<Object>();
        rowData.add(" ");
        rowData.add(" ");
        rowData.add(" ");
        rowData.add(" ");
        rowData.add(" ");
        rowData.add(" ");
        return rowData;
    }
    
    public static Vector<Object> getVector(PaperRecord record) {
        Vector<Object> rowData = new Vector<Object>();
        rowData.add(record.getTitle());
        rowData.add(record.getStatus());
        rowData.add(record.getAuthors());
        rowData.add(record.getLink());
        rowData.add(record.getConference());
        rowData.add(record.getTags());
        return rowData;
    }
    
    public static PaperRecord getPaper(Vector<Object> data) {
        PaperRecord record = new PaperRecord();
        Object[] array = data.toArray();
        record.setTitle(array[0].toString());
        record.setStatus(array[1].toString());
        record.setAuthors(array[2].toString());
        record.setLink(array[3].toString());
        record.setConference(array[4].toString());
        record.setTags(array[5].toString());
        return record;
    }

}
