package vn.com.hung.notetaker.datatypes;

public class PaperTags {
    
    public static final String ROOT_START = "<PAPERS>";
    public static final String ROOT_END = "</PAPERS>";
    
    public static final String PAPER_TAG_START = "<paper>";
    public static final String PAPER_TAG_END = "</paper>";
    public static final String PAPER_TAG = "paper";
    
    public static final String TITLE_TAG_START = "<title>";
    public static final String TITLE_TAG_END = "</title>";
    public static final String TITLE_TAG = "title";
    
    public static final String AUTHORS_TAG_START = "<authors>";
    public static final String AUTHORS_TAG_END = "</authors>";
    public static final String AUTHORS_TAG = "authors";
    
    public static final String LINK_TAG_START = "<link>";
    public static final String LINK_TAG_END = "</link>";
    public static final String LINK_TAG = "link";
    
    public static final String CONFERENCE_TAG_START = "<conf>";
    public static final String CONFERENCE_TAG_END = "</conf>";
    public static final String CONFERENCE_TAG = "conf";
    
    public static final String STATUS_TAG_START = "<status>";
    public static final String STATUS_TAG_END = "</status>";
    public static final String STATUS_TAG = "status";
    
    public static final String TAGS_TAG_START = "<tags>";
    public static final String TAGS_TAG_END = "</tags>";
    public static final String TAGS_TAG = "tags";
    
    public static final String CONTENT_TAG_START = "<content>";
    public static final String CONTENT_TAG_END = "<content>";
    public static final String CONTENT_TAG = "<content>";
    
    
    
    
    
    

}
