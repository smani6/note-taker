package vn.com.hung.notetaker.datatypes;

import vn.com.hung.notetaker.utils.Tags;

public class BookNoteRecord {
    
    private String term = "";
    private String contents = "";
    private String tags = "";
    private String misc = "";
    
    public BookNoteRecord() { }
    
    public String getTerm() {
        return term;
    }
    public String getContents() {
        return contents;
    }
    
    public String getTags() {
        return tags;
    }
    public String getMisc() {
        return misc;
    }
    
    public void setTerm(String term) {
        this.term = term;
    }
    public void setContents(String contents) {
        this.contents = contents;
    }
    public void setTags(String tags) {
        this.tags = tags;
        Tags.processTags(tags);
    }
    
    public void setMisc(String misc) {
        this.misc = misc;
    }
}
