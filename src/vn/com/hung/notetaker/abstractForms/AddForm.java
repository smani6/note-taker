package vn.com.hung.notetaker.abstractForms;

import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JTable;

@SuppressWarnings("serial")
public abstract class AddForm extends JFrame{
    public static final int EDIT_MODE = 1;
    public static final int ADD_MODE = 0;
    
    protected int mode = 0;
    protected JTable table;
    
    public AddForm(JTable table) {
        this.table = table;
    }
    
    public abstract void setData(Vector<Object> data); 
    public abstract void setMode(int mode);

}
