package vn.com.hung.notetaker.login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import vn.com.hung.notetaker.config.Conf;

public class PwdVerifier {
    
    public static boolean verify(String name, String pwd) {
        try {
            if (name.equalsIgnoreCase(Conf.getUserName())
                    && getMD5Code(pwd).equals(Conf.getPWD())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    public static String getMD5Code(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}
