package vn.com.hung.notetaker.login;



import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.book.forms.BookListViewForm;
import vn.com.hung.notetaker.config.Conf;
import vn.com.hung.notetaker.mis.forms.MiscForm;
import vn.com.hung.notetaker.paper.forms.PaperViewForm;
import vn.com.hung.notetaker.utils.DialogDisplayer;
import vn.com.hung.notetaker.utils.FontPool;
import vn.com.hung.notetaker.utils.Tags;

@SuppressWarnings("serial")
public class LoginForm extends JFrame implements ActionListener{
    
    private JLabel nameLabel = new JLabel("User Name");
    private JLabel pwdLabel = new JLabel("Password");
    private JLabel welcomeLabel = new JLabel("Welcome To Notaker 1.0  ---- Author: Hunglv");
    private JTextField nameField = new JTextField(20);
    private JPasswordField pwdField = new JPasswordField(20);
    private JButton bookButton = new JButton("Book Note");
    private JButton paperButton = new JButton("Paper Note");
    private JButton misButton = new JButton("Miscellaneous Note");
    
    public LoginForm() {
        Conf.startUp((Component)this);
        Tags.loadTag(Conf.getDBPath() + "/tags.txt");
        welcomeLabel.setFont(FontPool.LARGE_SERIF_BOLD);
        init();
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(welcomeLabel)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                .addComponent(nameLabel)
                                .addComponent(pwdLabel))
                         .addGroup(layout.createParallelGroup(Alignment.LEADING)
                                .addComponent(nameField)
                                .addComponent(pwdField)))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(bookButton)
                        .addComponent(paperButton)
                        .addComponent(misButton)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(welcomeLabel)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(nameLabel)
                        .addComponent(nameField))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(pwdLabel)
                        .addComponent(pwdField))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(bookButton)
                        .addComponent(paperButton)
                        .addComponent(misButton)));
        pack();
        setResizable(false);
        setLocation(300, 200);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    private void init() {
        bookButton.setName("Book");
        bookButton.addActionListener(this);
        paperButton.setName("Paper");
        paperButton.addActionListener(this);
        misButton.setName("Mis");
        misButton.addActionListener(this);
    }
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                new LoginForm().setVisible(true);
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        Tags.trigAutoSave();
        if ( !PwdVerifier.verify(nameField.getText(), pwdField.getText())) {
            DialogDisplayer.showDefaultErrorDialog("Please input corrent username and password");
            return;
        }
            
        String componentName = ((JComponent) arg0.getSource()).getName();
        if (componentName.equals(bookButton.getName())) {
            BookListViewForm note = new BookListViewForm();
            String dataPath = Conf.getBookNotePath() + "data.xml";
            if (new File(dataPath).exists()) {
                note.loadBookList(dataPath);
            }
            note.setVisible(true);
            dispose();
        } else if (componentName.equals(paperButton.getName())) {
            PaperViewForm note = new PaperViewForm();
            String dataPath = Conf.getPaperNotePath() + "data.xml";
            if (new File(dataPath).exists()) {
                note.loadPapers(dataPath);
            }
            note.setVisible(true);
        } else if (componentName.equals(misButton.getName())) {
            new MiscForm().setVisible(true);
        }
    }
}
