package vn.com.hung.notetaker.paper.forms;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout.Alignment;

import com.google.common.io.Closeables;
import com.google.common.io.Files;

import vn.com.hung.notetaker.config.Conf;
import vn.com.hung.notetaker.datatypes.PaperRecord;
import vn.com.hung.notetaker.utils.AutoJTextField;
import vn.com.hung.notetaker.utils.DialogDisplayer;
import vn.com.hung.notetaker.utils.FontPool;

@SuppressWarnings("serial")
public class PNForm extends JFrame implements ActionListener{ 
    
    private PaperRecord paper;
    private JLabel titleLabel = new JLabel("Title:");
    private JTextField titleTF = new JTextField();
    private JLabel authorLabel = new JLabel("Authors");
    private JTextField authorTF = new JTextField();
    private JLabel linkLabel = new JLabel("Link");
    private JTextField linkTF = new JTextField();
    private JLabel tagLabel = new JLabel("Tags");
    private AutoJTextField tagTF = new AutoJTextField();
    private JLabel content = new JLabel();
    private JScrollPane pane = new JScrollPane(content);
    private JButton editButton = new JButton("Edit");
    private JButton saveButton = new JButton("Save");
    
    public PNForm(PaperRecord paper) {
        this.paper = paper;
        
        initComponents();
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(titleLabel)
                        .addComponent(titleTF))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(authorLabel)
                        .addGap(30)
                        .addComponent(authorTF))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(linkLabel)
                        .addGap(55)
                        .addComponent(linkTF))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(tagLabel)
                        .addGap(52)
                        .addComponent(tagTF))
                .addComponent(pane)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(editButton)
                        .addGap(30)
                        .addComponent(saveButton)));
        
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGap(20)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(titleLabel)
                        .addComponent(titleTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(authorLabel)
                        .addComponent(authorTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(linkLabel)
                        .addComponent(linkTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(tagLabel)
                        .addComponent(tagTF))
                .addGap(10)
                .addComponent(pane)
                .addGap(10)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(editButton)
                        .addComponent(saveButton))
                 .addGap(10));
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    
    private void initComponents() {
        titleLabel.setFont(FontPool.LARGE_SERIF_BOLD);
        titleTF.setText(paper.getTitle());
        titleTF.setEditable(false);
        titleTF.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 0));
        titleTF.setFont(FontPool.LARGE_SERIF_BOLD);
        
        authorLabel.setFont(FontPool.MEDIUM_SERIF_BOLD);
        authorTF.setText(paper.getAuthors());
        authorTF.setEditable(false);
        authorTF.setFont(FontPool.MEDIUM_SERIF_BOLD);
        authorTF.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        
        linkTF.setText(paper.getLink());
        linkTF.setEditable(false);
        linkTF.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        
        tagTF.setText(paper.getTags());
        tagTF.setEditable(true);
        
        content.setVerticalAlignment(SwingConstants.TOP);
        content.setPreferredSize(new Dimension(700,300));
        String contentStr = paper.readContent();
        if (contentStr.length()> 0) {
            content.setText(contentStr);
        }
        content.setOpaque(true);
        content.setBackground(Color.white);
        content.setBorder(BorderFactory.createLineBorder(Color.gray));
        content.addMouseListener(new MyMouseListener());
        
        editButton.addActionListener(this);
        saveButton.addActionListener(this);
    }


    public static void main(String[] args) {
        new PNForm(new PaperRecord("Latent Dirichlet Allocation ", 
                "David Blei,Andrew Ng,Michael Jordan", 
                "http://www.cs.princeton.edu/~blei/papers/BleiNgJordan2003.pdf",
                "Journal of Machine Learning Research")).setVisible(true);
    }
    
    public String getSampleContent() {
        return "<html> <p> Contains extensions to the Swing GUI toolkit, including new and enhanced components that provide functionality commonly required by rich client applications. Highlights include: </p>"
                + "<ol> <li>Sorting, filtering, highlighting for tables, trees, and lists </li>"
                + "<li>Find/search</li>"
                + "<li>Auto-completion</li>"
                + "<li>Login/authentication framework</li>"
                + "<li>TreeTable component</li>"
                + "<li>Collapsible panel component</li>"
                + "<li>Date picker component</li>"
                + "<li>Tip-of-the-Day component</li> "
                + "</ol> <p>"
                + "Many of these features will eventually be incorporated into the Swing toolkit, although API compatibility will not be guaranteed. The SwingX project focuses exclusively on the raw components themselves"
                + "</p> </html>";
    }

    public String getContent() {
        return content.getText();
    }
    
    public void setContent(String txt) {
        content.setText(txt);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        JComponent comp = (JComponent) arg0.getSource();
        if (comp.equals(editButton)) {
            new PNEditFrame(content).setVisible(true);
        } else if (comp.equals(saveButton)) {
            BufferedWriter bw  = null;
            try {
                String path = Conf.getPaperNotePath();
                bw = Files.newWriter(new File(path + paper.getFileName()),
                        Charset.forName("UTF-8"));
                bw.write(content.getText());
            } catch (IOException e) {
                DialogDisplayer.showDefaultErrorDialog("Save Fail!");
            } finally {
                Closeables.closeQuietly(bw);
            }
            DialogDisplayer.showDefaultInforDialog("Save Done!");
            
        }
    }
    
    private class MyMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                
            } else if (SwingUtilities.isRightMouseButton(e)) {
                Point p = e.getPoint();
                
            }
        }
    }

}
