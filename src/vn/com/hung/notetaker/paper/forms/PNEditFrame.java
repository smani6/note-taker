package vn.com.hung.notetaker.paper.forms;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.utils.HTML2Text;

@SuppressWarnings("serial")
public class PNEditFrame extends JFrame implements ActionListener{
    
    private JLabel mainContent;
    private JTextArea textField = new JTextArea(10, 10);
    private JScrollPane pane = new JScrollPane(textField);
    private JButton button = new JButton("Done");

    public PNEditFrame(JLabel content) {
        setTitle("HTML Editor");
        this.mainContent = content;
        textField.setText(HTML2Text.convert(content.getText()));
        textField.setLineWrap(true);
        textField.setWrapStyleWord(true);
        button.addActionListener(this);
        pane.setPreferredSize(new Dimension(500, 300));
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(pane)
                .addComponent(button,Alignment.CENTER));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(pane)
                .addComponent(button));
        
        pack();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
       mainContent.setText(textField.getText());
       dispose();
    }

}
