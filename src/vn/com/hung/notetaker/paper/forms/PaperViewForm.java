package vn.com.hung.notetaker.paper.forms;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.abstractMenus.NoteViewMenu;
import vn.com.hung.notetaker.abstractMenus.TableSelectedPopupMenu;
import vn.com.hung.notetaker.datatypes.PaperRecord;
import vn.com.hung.notetaker.paper.datamodels.PaperViewModel;

@SuppressWarnings("serial")
public class PaperViewForm extends JFrame implements ActionListener{
    
    private JButton addButton = new JButton("Add Paper");
    private JButton openButton = new JButton("Open");
    private JButton saveButton = new JButton("Save");
    
    private JTable paperTable = new JTable();
    private NoteViewMenu menu = new NoteViewMenu(paperTable);
    private JScrollPane tablePane = new JScrollPane(paperTable);
    public PaperViewForm() {
        setTitle("Paper Note Taker 1.0 @ Hunglv");
        setJMenuBar(menu);
        initComponents();
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(tablePane)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(addButton)
                        .addComponent(openButton)
                        .addComponent(saveButton)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(tablePane)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(addButton)
                        .addComponent(openButton)
                        .addComponent(saveButton)));
        
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    
    private void initComponents() {
        paperTable.setModel(new PaperViewModel());
        paperTable.getColumnModel().getColumn(0).setPreferredWidth(600);
        paperTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        paperTable.addMouseListener(new TableMouseListener());
        addButton.addActionListener(this);
        openButton.addActionListener(this);
        saveButton.addActionListener(this);
        
    }


    @Override
    public void actionPerformed(ActionEvent arg0) {
        System.out.println("action performed");
        JButton component = (JButton) arg0.getSource();
        if (component.equals(addButton)) {
            new AddPaperForm(paperTable);
        } else if (component.equals(openButton)) {
           if (paperTable.getSelectedRow() > -1) {
               Vector<Object> rowData = ((PaperViewModel)paperTable.getModel()).
                       getRowData(paperTable.getSelectedRow());
               new PNForm(PaperRecord.getPaper(rowData)).setVisible(true);
                  
           }
            
        } else if (component.equals(saveButton)) {
            System.out.println("save");
        }
    }
    public void loadPapers(String path) {
        try {
            ((PaperViewModel) paperTable.getModel()).loadCSV(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        paperTable.updateUI();
    }
    
    public static void main(String[] args) {
        new PaperViewForm().setVisible(true);
    }
    private class TableMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
               
            } else if (SwingUtilities.isRightMouseButton(e)) {
                Point p = e.getPoint();
                int row = paperTable.rowAtPoint(p);
                int column = paperTable.columnAtPoint(p);
                if (row == paperTable.getSelectedRow()) {
                    TableSelectedPopupMenu pmenu = new TableSelectedPopupMenu(paperTable);                
                    pmenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }
}
