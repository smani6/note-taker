package vn.com.hung.notetaker.paper.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.paper.datamodels.PaperViewModel;
import vn.com.hung.notetaker.utils.AutoJTextField;
import vn.com.hung.notetaker.utils.Tags;

@SuppressWarnings("serial")
public class AddPaperForm extends AddForm implements ActionListener{
    
    private String[] statusList = {"N/A", "Reading", "Completed"};
    private JLabel paperAuthor = new JLabel("Author");
    private JTextField paperAuthorTF = new JTextField(60);
    private JLabel paperNameLB = new JLabel("Paper Name");
    private JTextField paperNameTF = new JTextField(60);
    private JLabel linkLB = new JLabel("Link");
    private JTextField linkTF  = new JTextField(60);
    private JLabel confLB = new JLabel("Conference");
    private JTextField confTF = new JTextField(60);
    private JLabel tagLB = new JLabel("Tag");
    private AutoJTextField tagTF = new AutoJTextField();
    private JLabel statusLB = new JLabel("Status");
    private JComboBox statusJCB = new JComboBox(statusList);
    private JButton okButton = new JButton("Ok");
    
    public AddPaperForm(JTable paperTable) {
       super(paperTable);
       initComponents();
       
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                        .addComponent(paperNameLB)
                        .addComponent(paperAuthor)
                        .addComponent(linkLB)
                        .addComponent(confLB)
                        .addComponent(tagLB)
                        .addComponent(statusLB)
                        .addComponent(okButton))
                .addGroup(layout.createParallelGroup(Alignment.LEADING)
                        .addComponent(paperNameTF)
                        .addComponent(paperAuthorTF)
                        .addComponent(linkTF)
                        .addComponent(confTF)
                        .addComponent(tagTF)
                        .addComponent(statusJCB)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(paperNameLB)
                        .addComponent(paperNameTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(paperAuthor)
                        .addComponent(paperAuthorTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(linkLB)
                        .addComponent(linkTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(confLB)
                        .addComponent(confTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(tagLB)
                        .addComponent(tagTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(statusLB)
                        .addComponent(statusJCB))
                .addGap(10)
                .addComponent(okButton));
        setVisible(true);
        setLocation(500, 200);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    
    
    private void initComponents() {
        
        statusJCB.setSelectedIndex(0);
        okButton.addActionListener(this);
       
    }



    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                AddPaperForm form =  new AddPaperForm(null);
                form.setLocation(500, 200);
            }
        });
    }
    
    public void setMode(int mode) {
        this.mode = mode;
        if (mode == ADD_MODE) {
            okButton.setText("Add");
        } else if (mode == EDIT_MODE) {
            okButton.setText("Done");
        }
    }
    
    public void setPaperNameField(String name) {
        this.paperNameTF.setText(name);
    }
    
    public void setAuthorField(String author) {
        this.paperAuthorTF.setText(author);
    }
    
    public void setStatusField(String status) {
        if (status == null || status.length() < 1) {
            statusJCB.setSelectedIndex(0);
            return;
        }
        ComboBoxModel model = statusJCB.getModel();
        for (int i = 0; i < model.getSize(); i++) {
            if (model.getElementAt(i).toString().equals(status)) {
                statusJCB.setSelectedIndex(i);
                return;
            }
        }
        
    }
    
    public void setLinkField(String link) {
        this.linkTF.setText(link);
    }
    
    public void setConferenceField(String confer) {
        this.confTF.setText(confer);
    }
    
    public void setTagField(String tags) {
        this.tagTF.setText(tags);
    }
    
    



    @Override
    public void actionPerformed(ActionEvent arg0) {
        String author = (paperAuthorTF.getText() == null || paperAuthorTF.getText().length() == 0 ) ? "N/A" : paperAuthorTF.getText();
        String status = statusJCB.getSelectedItem().toString();
        String link = (linkTF.getText() == null || linkTF.getText().length() == 0) ? "N/A" : linkTF.getText();
        String conference = (confTF.getText()== null || confTF.getText().length() == 0) ? "N/A" : confTF.getText();
        String tags = (tagTF.getText() == null || tagTF.getText().length() == 0)? "N/A" : tagTF.getText();
        String name = paperNameTF.getText();
        if (name == null || name.length() == 0) {
            name = " ";
            author = " ";
            status = " ";
            link = " ";
            conference = " ";
            tags =  " ";
            name = " ";
        }
        
        Vector<Object> data = new Vector<Object>();
        data.add(name);
        data.add(status);
        data.add(author);
        data.add(link);
        data.add(conference);
        data.add(tags);
        Tags.processTags(tags);
        if (mode == ADD_MODE) {
            ((PaperViewModel) table.getModel()).addRow(data, table.getRowCount());
        } else if (mode == EDIT_MODE) {
            ((PaperViewModel) table.getModel()).replaceRow(data, table.getSelectedRow());
        }
        dispose();
    }



    @Override
    public void setData(Vector<Object> data) {
        Iterator<Object> it = data.iterator();
        setPaperNameField(it.next().toString().trim());
        setStatusField(it.next().toString().trim());
        setAuthorField(it.next().toString().trim());
        setLinkField(it.next().toString().trim());
        setConferenceField(it.next().toString().trim());
        setTagField(it.next().toString().trim());
        
    }

}
