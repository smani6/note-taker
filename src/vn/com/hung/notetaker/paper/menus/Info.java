package vn.com.hung.notetaker.paper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;

import vn.com.hung.notetaker.paper.datamodels.PaperViewModel;
import vn.com.hung.notetaker.utils.XMLFilter;

@SuppressWarnings("serial")
public class Info extends JMenuBar implements ActionListener{
    
    private JTable table;
    
    private JMenu fileMenu = new JMenu("File");
    private JMenuItem loadItem = new JMenuItem("Load Data");
    private JMenuItem saveAsItem = new JMenuItem("Save As");
    
    
    public Info(JTable paperTable) {
        this.table = paperTable;
        add(fileMenu);
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(loadItem);
        fileMenu.add(saveAsItem);
        loadItem.setMnemonic(KeyEvent.VK_L);
        loadItem.setName("load");
        loadItem.addActionListener(this);
        
        saveAsItem.setMnemonic(KeyEvent.VK_S);
        saveAsItem.setName("saveAs");
        saveAsItem.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent arg0) {
        JComponent comp = (JComponent) arg0.getSource();
        
        if (comp.getName().equals(loadItem.getName())) {
            // process choose file
            JFileChooser c = new JFileChooser();
            c.setFileFilter(new XMLFilter());
            int returnValue = c.showOpenDialog(getParent());
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = c.getSelectedFile();
                try {
                    ((PaperViewModel) table.getModel()).loadCSV(selectedFile.getAbsolutePath());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                table.updateUI();
              }
        } else if (comp.getName().equals(saveAsItem.getName())) {
            // process save as 
            JFileChooser c = new JFileChooser();
            c.setFileFilter(new XMLFilter());
            int returnValue = c.showSaveDialog(getParent());
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                String fileName = c.getSelectedFile().getPath();
                if (!fileName.endsWith(".xml")) {
                    fileName += ".xml";
                }
                try {
                    ((PaperViewModel) table.getModel()).saveToCSV(fileName);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    
   
    

}
