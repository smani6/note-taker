package vn.com.hung.notetaker.paper.datamodels;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;

import com.google.common.io.Closeables;
import com.google.common.io.Files;


import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.abstractModels.NoteTableModel;
import vn.com.hung.notetaker.datatypes.BookRecord;
import vn.com.hung.notetaker.datatypes.PaperRecord;
import vn.com.hung.notetaker.datatypes.PaperTags;
import vn.com.hung.notetaker.paper.forms.AddPaperForm;
import vn.com.hung.notetaker.utils.BookDataLoader;
import vn.com.hung.notetaker.utils.PaperDataLoader;


@SuppressWarnings({ "unchecked", "serial" })
public class PaperViewModel extends NoteTableModel {
    
    
    public PaperViewModel() {
        String[] columns = {"Paper's Name", "Status"};
        this.columnNames = columns;
        
        for (int i = 0; i < 50; i++) {
            Vector<Object> rowData = PaperRecord.getEmptyRow();
            data.add(rowData);
        }
    }
    public PaperViewModel(String[] columnNames) {
        this.columnNames = columnNames;
        for (int i = 0; i < 50; i++) {
            Vector<Object> rowData = PaperRecord.getEmptyRow();
            data.add(rowData);
        }
    }
    
    public PaperViewModel(String[] columnNames, Vector<Vector<Object>> data) {
        this.columnNames = columnNames;
        this.data = data;
        
    }

    @Override
    public void loadCSV(String path) throws IOException {
        this.filePath = path;
        loader = new PaperDataLoader(path);
        loader.load();
        List<PaperRecord> papers = ((PaperDataLoader)loader).getPapers();
        data.clear();
        for (PaperRecord paper : papers) {
            Vector<Object> datum = new Vector<Object>();
            datum.add(paper.getTitle());
            datum.add(paper.getStatus());
            datum.add(paper.getAuthors());
            datum.add(paper.getLink());
            datum.add(paper.getConference());
            datum.add(paper.getTags());
            data.add(datum);
        }
        
    }

    @Override
    public void saveToCSV(String fileName) throws IOException {
        BufferedWriter bw = Files.newWriter(new File(fileName), Charset.forName("UTF-8"));
        if (data == null || data.size() == 0) {
            Closeables.closeQuietly(bw);
            return;
        }
        bw.write(PaperTags.ROOT_START + "\n");
        for (Vector<Object> datum : data) {
            bw.write(PaperTags.PAPER_TAG_START + "\n");
            Iterator<Object> it = datum.iterator();
            for (int i = 0; it.hasNext(); i++) {
                switch (i) {
                case 0:
                    bw.write(PaperTags.TITLE_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.TITLE_TAG_END + "\n");
                    break;
                case 1:
                    bw.write(PaperTags.STATUS_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.STATUS_TAG_END + "\n");
                    break;
                case 2: 
                    bw.write(PaperTags.AUTHORS_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.AUTHORS_TAG_END + "\n");
                    break;
                case 3:
                    bw.write(PaperTags.LINK_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.LINK_TAG_END + "\n");
                    break;
                case 4: 
                    bw.write(PaperTags.CONFERENCE_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.CONFERENCE_TAG_END + "\n");
                    break;
                case 5:
                    bw.write(PaperTags.TAGS_TAG_START);
                    bw.write(it.next().toString());
                    bw.write(PaperTags.TAGS_TAG_END + "\n");
                    break;
                default:
                    break;
                    
                }
                  
            }
            bw.write(PaperTags.PAPER_TAG_END + "\n");
        }
        bw.write(PaperTags.ROOT_END);
        Closeables.closeQuietly(bw);
    }

    @Override
    public int getStatusRowId() {
        return 1;
    }
    @Override
    public AddForm getEditForm(JTable table) {
        return new AddPaperForm(table);
    }
    
   
}
