package vn.com.hung.notetaker.mis.forms;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import vn.com.hung.notetaker.mis.menus.MisMenu;

@SuppressWarnings("serial")
public class MiscForm extends JFrame implements ActionListener{
    
    
    private JTabbedPane tabbedPane = new JTabbedPane();
    private JButton saveButton = new JButton("Save");
    private JButton newPageButton = new JButton("New Page");
    private JButton addLine = new JButton("Add Line");
    private ArrayList<JPanel> panels = new ArrayList<JPanel>();
    public MiscForm() {
        setTitle("Miscellaneous Note Hunglv@ V1.0");
        JPanel panel = new MiscPanel(100);
        panels.add(panel);
        
        initComponents();
        setJMenuBar(new MisMenu(this));
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        
        tabbedPane.addTab("Page 1", getScroll(panel));
        
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(tabbedPane)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(saveButton)
                        .addComponent(newPageButton)
                        .addComponent(addLine)));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(tabbedPane)
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(saveButton)
                        .addComponent(newPageButton)
                        .addComponent(addLine)));
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
    }
    
    public void initComponents() {
        tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
        tabbedPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        tabbedPane.addChangeListener(new MyChangeListener());
        newPageButton.addActionListener(this);
        saveButton.addActionListener(this);
        addLine.addActionListener(this);
    }
    
    public JScrollPane getScroll(JPanel panel) {
        JScrollPane scroll = new JScrollPane(panel);
        scroll.setPreferredSize(new Dimension(1000, 500));
        return scroll;
    }
    
    public void addTab(String title) {
        JPanel panel = new MiscPanel(100);
        panels.add(panel);
        tabbedPane.add(title, getScroll(panel));
    }

    public void load(File file) {
        getWorkingPanel().load(file);
    }
    public void saveXML(String path) {
        getWorkingPanel().saveXML(path);
    }
    
    public MiscPanel getWorkingPanel() {
        return (MiscPanel)panels.get(tabbedPane.getSelectedIndex());
    }
    public static void main(String[] args) {
        new MiscForm().setVisible(true);
    }
    
    

    @Override
    public void actionPerformed(ActionEvent arg0) {
        JComponent source = (JComponent) arg0.getSource();
        if (source.equals(newPageButton)) {
            addTab("Page " + (panels.size() + 1));
        } else if (source.equals(saveButton)) {
            getWorkingPanel().saveContextXML();           
        } else if (source.equals(addLine)) {
            getWorkingPanel().addLines(1);

        }
    }
    
    private class MyChangeListener implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            System.out.println(tabbedPane.getSelectedIndex());
            
        }
        
    }
    

}
