package vn.com.hung.notetaker.mis.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;

import vn.com.hung.notetaker.mis.datamodels.NoteLine;
import vn.com.hung.notetaker.utils.AutoJTextField;

@SuppressWarnings("serial")
public class AttributeEditFrame extends JFrame implements ActionListener{
    
    private NoteLine line;
    private JLabel tagsLB = new JLabel("Tags");
    private AutoJTextField tagsTF = new AutoJTextField(30);
    private JLabel linkLB = new JLabel("Link");
    private JTextField linkTF = new JTextField(30);
    private JButton okButton = new JButton("Done");
    public AttributeEditFrame(NoteLine line) {
        setTitle("Add Attribute for Note Hunglv@");
        this.line = line;
        okButton.addActionListener(this);
        linkTF.setText(line.getLink());
        tagsTF.setText(line.getTags());
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(Alignment.LEADING)
                            .addComponent(tagsLB)
                            .addComponent(linkLB))
                            .addGroup(layout.createParallelGroup(Alignment.LEADING)
                            .addComponent(tagsTF)
                            .addComponent(linkTF)))
               .addComponent(okButton, Alignment.CENTER));
        
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(tagsLB)
                        .addComponent(tagsTF))
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(linkLB)
                        .addComponent(linkTF))
                 .addComponent(okButton));
        pack();
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
    }
    
    public static void main(String[] args) {
        new AttributeEditFrame(new NoteLine()).setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent arg0) {
        JComponent source = (JComponent) arg0.getSource();
        if (source.equals(okButton)) {
            if (tagsTF.getText()!=null) {
                line.setTags(tagsTF.getText());
                
            } 
            if (linkTF.getText() != null) {
                line.setLink(linkTF.getText());
            }
            setVisible(false);
        }
    }

}
