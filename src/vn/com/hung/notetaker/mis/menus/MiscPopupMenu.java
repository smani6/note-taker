package vn.com.hung.notetaker.mis.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import vn.com.hung.notetaker.mis.datamodels.NoteLine;
import vn.com.hung.notetaker.mis.forms.AttributeEditFrame;
;

public class MiscPopupMenu extends JPopupMenu implements ActionListener{
    
    private NoteLine line;
    private JMenuItem attribute = new JMenuItem("Add Info", KeyEvent.VK_A);
    private JMenuItem clear = new JMenuItem("Clear", KeyEvent.VK_C);
    public MiscPopupMenu(NoteLine line) {
        this.line = line;
        initComponents();
        add(attribute);
        add(clear);
    }

    private void initComponents() {
        attribute.addActionListener(this);
        clear.addActionListener(this);
        
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        JComponent source = (JComponent) arg0.getSource();
        if (source.equals(clear)) {
            line.setText(" ");
        } else if (source.equals(attribute)) {
            new AttributeEditFrame(line).setVisible(true);
        }
    }
    

}
