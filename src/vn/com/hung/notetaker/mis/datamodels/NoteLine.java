package vn.com.hung.notetaker.mis.datamodels;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JTextArea;

import vn.com.hung.notetaker.utils.FontPool;
import vn.com.hung.notetaker.utils.Tags;

@SuppressWarnings("serial")
public class NoteLine extends JTextArea{
    
    private String tags = "";
    private String link = "";
   
    
    public NoteLine() {
        setLineWrap(true);
        setWrapStyleWord(true);
        setText(" ");
        setBorder(BorderFactory.createLineBorder(Color.blue));
        setFont(FontPool.MEDIUM_SERIF_PLAIN);
        
    }
    
    public String getTags() {
        return tags;
    }
    
    public String getLink() {
        return link;
    }
    
    public void setTags(String tags) {
        this.tags = tags;
        setToolTipText("Tags: " + tags);
        Tags.processTags(tags);
    }
    
    public void setLink(String link) {
        this.link = link;
    }
    
    

}
