package vn.com.hung.notetaker.abstractModels;

import java.io.IOException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.utils.AbstractLoader;

@SuppressWarnings("serial")
public abstract class NoteTableModel extends AbstractTableModel {

    protected String filePath;
    protected String[] columnNames;
    protected Vector<Vector<Object>> data = new Vector<Vector<Object>>();
    protected AbstractLoader loader;
    
    
    public abstract void loadCSV(String path) throws IOException;
    public abstract void saveToCSV(String fileName) throws IOException;
    public abstract int getStatusRowId(); 
    public abstract AddForm getEditForm(JTable table);

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        return data.get(row).get(col);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public Vector<Object> getRowData(int row) {
        return data.get(row);
    }

    public void setValueAt(Object value, int row, int col) {
        data.get(row).set(col, value);
        fireTableCellUpdated(row, col);
    }

    public void addRow(Vector<Object> rowData, int row) {
        this.data.insertElementAt(rowData, row);
        fireTableRowsInserted(row, row + 1);
    }

    public void replaceRow(Vector<Object> rowData, int row) {
        this.data.remove(row);
        this.data.insertElementAt(rowData, row);
        fireTableRowsInserted(row, row + 1);
    }

    public void deleteRow(int row) {
        this.data.remove(row);
        fireTableRowsDeleted(row, row + 1);
    }
    
    public String getModelFilePath() {
        return filePath;
    }
    
    

}
