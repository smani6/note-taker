package vn.com.hung.notetaker.abstractMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

import vn.com.hung.notetaker.abstractForms.AddForm;
import vn.com.hung.notetaker.abstractModels.NoteTableModel;
import vn.com.hung.notetaker.book.datamodels.BookViewTableModel;
import vn.com.hung.notetaker.book.forms.AddBookForm;

@SuppressWarnings("serial")
public class TableSelectedPopupMenu extends JPopupMenu implements ActionListener{
    
    private JTable parentTable;
    private JMenuItem deleteMenuItem = new JMenuItem("Delete");
    private JMenuItem editMenuItem = new JMenuItem("Edit");
    
    private JMenu insertRow = new JMenu("Insert Row");
    private JMenuItem rowBelowItem = new JMenuItem("Row Below", KeyEvent.VK_B);
    private JMenuItem rowAboveItem = new JMenuItem("Row Above", KeyEvent.VK_A);
    
    private JMenu markMenu = new JMenu("Mark As");
    private JMenuItem markAsComplete = new JMenuItem("Complete", KeyEvent.VK_C);
    private JMenuItem markAsReading = new JMenuItem("Reading", KeyEvent.VK_R);
    
    
    public TableSelectedPopupMenu(JTable parentTable) {
        this.parentTable = parentTable;
        
        //process mark menu
        markAsComplete.setName("markAsComplete");
        markAsComplete.addActionListener(this);
        markAsReading.setName("markAsReading");
        markAsReading.addActionListener(this);
        markMenu.add(markAsComplete);
        markMenu.add(markAsReading);
        // process insert row menu
        rowBelowItem.setName("rowBelow");
        rowBelowItem.addActionListener(this);
        rowAboveItem.setName("rowAbove");
        rowAboveItem.addActionListener(this);
        
        insertRow.setMnemonic(KeyEvent.VK_I);
        insertRow.add(rowBelowItem);
        insertRow.add(rowAboveItem);
        
        // process editMenuItem
        deleteMenuItem.setName("delete");
        deleteMenuItem.addActionListener(this);
        editMenuItem.setName("edit");
        editMenuItem.addActionListener(this);
        
        add(editMenuItem);
        add(deleteMenuItem);
        add(markMenu);
        add(insertRow);
        
        
    }
    
   @Override
    public void actionPerformed(ActionEvent arg0) {
       int selectedRow = parentTable.getSelectedRow();
        JComponent component = (JComponent) arg0.getSource();
        String compName = component.getName();
        NoteTableModel model = (NoteTableModel) parentTable.getModel();
        if (compName.equalsIgnoreCase(rowBelowItem.getName())) {
            Vector<Object> rowData = new Vector<Object>();
            for (int i = 0; i < model.getColumnCount(); i++) {
                rowData.add("");
            }
            model.addRow(rowData, selectedRow + 1);
        
        } else if (compName.equalsIgnoreCase(rowAboveItem.getName())) {
            Vector<Object> rowData = new Vector<Object>();
            for (int i = 0; i < model.getColumnCount(); i++) {
                rowData.add("");
            }
            model.addRow(rowData, selectedRow );
        
        } else if (compName.equalsIgnoreCase(markAsComplete.getName())) {
            if (parentTable.getValueAt(selectedRow, 0).toString().length() == 0) {
                return;
            }
            parentTable.setValueAt("Completed", selectedRow, model.getStatusRowId());
            
        } else if (compName.equalsIgnoreCase(markAsReading.getName())) {
            if (parentTable.getValueAt(selectedRow, 0).toString().length() == 0) {
                return;
            }
            parentTable.setValueAt("Reading", selectedRow, model.getStatusRowId());
        } else if (compName.equals(deleteMenuItem.getName())) {
            model.deleteRow(selectedRow);
            
        } else if (compName.equals(editMenuItem.getName())) {
            AddForm form = model.getEditForm(parentTable);
            form.setMode(AddForm.EDIT_MODE);
            Vector<Object> rowData = ((NoteTableModel) parentTable
                    .getModel()).getRowData(parentTable.getSelectedRow());
           form.setData(rowData);
        }
        
    }
}
