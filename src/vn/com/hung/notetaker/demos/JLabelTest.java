package vn.com.hung.notetaker.demos;

import javax.swing.JButton;
import javax.swing.JFrame;

public class JLabelTest extends JFrame{
    
    JButton label = new JButton("label");
    public JLabelTest() {
        add(label);
        label.setText("<html><b><u>T</u>wo</b><br>lines</html>");
        setBounds(100, 100, 100, 100);
        setVisible(true);
        setEnabled(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
    }
    
    public static void main(String[] args) {
        new JLabelTest();
    }

}
